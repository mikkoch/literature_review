---
title: "eSVD"
output:
   workflowr::wflow_html:
    toc: false
---

## Probablistic Model

$$\theta_{ij} = \boldsymbol{\phi}_i'\boldsymbol{m}_j$$
$$\log (y_{ij} \lvert \theta) = y_{ij}\theta_{ij} - h(\theta_{ij}) + c(y_{ij})$$

where $\boldsymbol{\phi}_i$ and $\boldsymbol{m}_j$ are both length-$r$ vectors. $i$ indexes over $1, 2, \ldots, p$ genes; $j$ indexes over $1, 2, \ldots, n$ samples. Their goal is to estimate $\boldsymbol{\phi_i}$ and $\boldsymbol{m}_j$.

## Inference 

The loss function 
\begin{align}
L = \sum_{ij}\left(h(\theta_{ij}) - y_{ij}\theta_{ij}\right)
\end{align}
Then, for iterations $t \in \{0, 1, \ldots, T - 1\}$
<ol>
  <li>$\tilde{M}^{(t + 1)} = \arg\min_{M \in R^{r\times n}} L(M, \Phi^{(t)})$</li>
  <li>$M^{(t+1)} = \sqrt{n}\text{RightSVD}(\tilde{M}^{(t+1)})$ </li>
  <li>$\tilde{\Phi}^{(t + 1)} = \arg\min_{\Phi \in R^{p\times r}} L(M^{(t+1)}, \Phi)$</li>
  <li>$\Phi^{(t+1)} = \sqrt{p}\text{LeftSVD}(\tilde{\Phi}^{(t+1)})$ </li>
</ol>





