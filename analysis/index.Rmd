---
title: "Literature Review for Single cell"
output:
   workflowr::wflow_html:
    toc: true
    toc_float: true
editor_options:
  chunk_output_type: console
bibliography: bibliography.bib
csl: biomed-central.csl
link-citations: yes
linkcolor: blue
site: workflowr::wflow_site
---

## Properties of single cell sequencing data 

<ol>
  <li>Percentages of measurements reported as zero ranging <strong>from 35% to 99%</strong> across cells [@hicks2018missing] </li> 
  <ul>
    <li> <strong><em>structural zeros</em></strong>: the gene was not expressing any RNA at the time the cell was experimentally isolated and processed prior sequencing</li>
    <li> <strong><em>dropouts</em></strong>: the gene was expressing RNA in the cell at the time of isolation, but due to the limitations of current experimental protocols to detect low amounts of RNA in a cell, the gene was not detected</li>
  </ul>
  <li>Proportion of zeros varies greatly from cell to cell</li>
  <li>Some of these reported zeros are driven by <strong>technical variation</strong></li> 
  <ul>
    <li> Detection rates correlate with first few principal components (PCs) </li>
  </ul>
  <li> How to model detection rate?</li>
  <ul>
    <li> <strong><em>logistic regression</em></strong>: $\pi_{ij} = \frac{1}{1 + \exp(-(\alpha_{0j} + \alpha_{1j}\theta_{ij}))}$, where $i$ is index for genes and $j$ is index for cells [@kharchenko2014bayesian; @hicks2018missing]</li>
    <li> <strong><em>ZIFA model</em></strong>: $\pi_{ij} = e^{-\alpha \theta_{ij}^2}$ [@pierson2015zifa]</li>
  </ul>
</ol>

## Testing differential expression 

## Latent variable estimations 

In sc-RNA data analysis, people are generally interested in estimating the following model
$$
\begin{aligned}
\theta_{ij} = \boldsymbol{\beta} _i'\boldsymbol{x}_j + \boldsymbol{\phi}_i'\boldsymbol{m}_j &\text{ or } \theta_{ij} = \boldsymbol{\phi}_i'\boldsymbol{m}_j \\
y_{ij} &\sim F(\theta_{ij})
\end{aligned}
$$
where $F$ can be an exponential family distribution (eSVD, GLM-PCA) or a non-standard distribution that accounts for sparsity of sc-RNA data (ZIFA, ZINB-WaVE, VAMF, pCMF). The primary reason for studying this model is feature extraction or data compression. 

| Methods | Probabilistic Models | Estimation | Asymptotic Properties | Comments |
|-|-|-|-|-|
| Zero Inflated Factor Analysis (**ZIFA**) [@pierson2015zifa][[details](https://mikkoch.gitlab.io/literature_review/zifa.html)] | $\boldsymbol{m}_j \sim N(\boldsymbol{0}, \boldsymbol{I})$ <br> $\theta_{ij} = \beta_i + \boldsymbol{\phi}_i'\boldsymbol{m}_j$ <br> $h_{ij} \sim N(\theta_{ij}, \sigma_{i}^2)$ <br> $Pr(Z_{ij} = 1 \lvert h_{ij}) = 1 - \exp(-\alpha h_{ij}^2)$ <br> $y_{ij} = h_{ij}$ if $Z_{ij} = 1$ otherwise $0$  | EM algorithm  | N/A |  |
| Varying-Censoring Aware Matrix Factorization (**VAMF**) [@townes2017varying] | $\theta_{ij} = \theta_0 + \beta_i + \boldsymbol{\phi}_i'\boldsymbol{m}_j$ <br> $\pi_{ij} = P(Z_{ij} = 1 \lvert \theta_{ij}) = \sigma(\alpha_{0j} + \alpha_{1j}\theta_{ij})$  <br> $\log_2(y_{ij})  \lvert Z_{ij} = 1 \sim N(\theta_{ij}, \sigma_i^2)$ | Automatic Differential Variational Inference (ADVI) algorithm  | N/A | The main difference between ZIFA and VAMF lies in the probabilistic model for $P(Z_ {ij} = 1 \lvert \theta_{ij})$. VAMF argues by using only one parameter for censoring, $\alpha$, the  ZIFA model is unable to capture the variations in proportion of sparsity across cells.  |
| Zero-inflated Negative Binomial Model (**ZINB-WaVE**) [@risso2018general][[details](https://mikkoch.gitlab.io/literature_review/zinb-wave.html)] | $\theta_{ij} = \left(\boldsymbol{\beta}_i^{\theta}\right)'\boldsymbol{x}_j + \left(\boldsymbol{\phi}_i^{\theta}\right)'\boldsymbol{m}_j$ <br> $\text{logit}\left(\frac{\pi_{ij}}{1 - \pi_{ij}} \right) = \left(\boldsymbol{\beta}_i^{\pi}\right)'\boldsymbol{x}_j + \left(\boldsymbol{\phi}_i^{\pi}\right)'\boldsymbol{m}_j$<br> $\mu_{ij} = \exp(\theta_{ij})$ <br> $y_{ij} \sim \pi_{ij}\delta_0(y_{ij}) + (1 - \pi_{ij})f_{\text{NB}}(y_{ij} \lvert \mu_{ij}, \gamma_{ij})$ | Alternative minimization | N/A | ZIFA and VAMF model detection rate as censoring; while ZINB-WaVE models detection rate as a parameter of intere |
| Generalized Principal Component Analysis (**GLM-PCA**) [@townes2019generalized][[details](https://mikkoch.gitlab.io/literature_review/glm-pca.html )] | $\theta_{ij} = \boldsymbol{\beta} _i'\boldsymbol{x}_j + \boldsymbol{\phi}_i'\boldsymbol{m}_j$ <br> $y_{ij} \lvert \theta_{ij} \sim \text{Exp-Fam}(\theta_{ij})$ | Alternative minimization | N/A |  |
| Probabilistic Count Matrix Factorization (**pCMF**) [@durif2019probabilistic][[details](https://mikkoch.gitlab.io/literature_review/pcmf.html)] | $m_{lj} \sim \Gamma(\alpha_{j1}, \alpha_{j2})$ <br> $\phi_{il} \sim (1 - S_{il}\delta_0 + S_{il}\Gamma(\beta_{i1}, \beta_{i2})$ <br> $\theta_{ij} = \phi_i'm_j$ <br> $Z_{ij} \sim \text{Bern}(\pi_j)$ <br> $Y_{ij} \sim (1 - Z_{ij}) \times \delta_0 + Z_{ij} \text{Poisson}(\phi_i'm_j)$ | Variational EM algorithm | N/A  |  |
| exponential-family SVD (**eSVD**) [@lin2021exponential][[details](https://mikkoch.gitlab.io/literature_review/esvd.html)] | $\theta_{ij} = \boldsymbol{\phi}_i'\boldsymbol{m}_j$ <br> $y_{ij} \lvert \theta_{ij} \sim \text{Exp-Fam}(\theta_{ij})$ | Alternative minimization |  |  |

## References
